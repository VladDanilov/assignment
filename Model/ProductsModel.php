<?php

namespace Model;

use core\DBConnector;

class ProductsModel extends BaseModel
{
  use \core\Traits\Singleton;

  protected static $instance;

  protected function __construct(\PDO $db)
  {
    parent::__construct($db, 'product');
  }

  public function getAll()
	{
		$sql = sprintf('SELECT * FROM %s', $this->table);
		$stmt = $this->db->query($sql);

    return $stmt->fetchAll();
  }

  public function insert($SKU, $name, $price, $size, $height, $width, $length, $weight)
  {
    $sql = sprintf("INSERT INTO %s (`SKU`, `name`, `price`, `size`, `height`, `width`, `length`, `weight`) VALUES (:SK, :na, :pri, :si, :he, :wi, :le, :we)", $this->table);
    $query = $this->db->prepare($sql);
    $query->execute(
      [
        'SK' => $SKU,
        'na' => $name,
        'pri' => $price,
        'si' => $size,
        'he' => $height,
        'wi' => $width,
        'le' => $length,
        'we' => $weight
      ]
    );
    DBConnector::checkQuery($query);
    return true;
  }

//Tryied to create delete action, but it does not work properly

  public function delete($fname)
  {
    $sql = sprintf("DELETE FROM %s WHERE `id`=:fname", $this->table);
    $query = $this->db->prepare($sql);
    $query->execute(
      [
        'fname' => $fname
      ]
    );
    DBConnector::checkQuery($query);
    return true;
  }
}
