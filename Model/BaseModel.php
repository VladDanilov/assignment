<?php

namespace Model;

abstract class BaseModel
{
	protected $db;
	protected $table;

	public function __construct(\PDO $db, $table)
	{
		$this->db = $db;
		$this->table = $table;
	}

	public function getAll()
	{
		$sql = sprintf('SELECT * FROM %s', $this->table);
		$stmt = $this->db->query($sql);

		return $stmt->fetchAll();
	}

	public function getOne($fname)
	{
		$sql = sprintf('SELECT * FROM %s WHERE id = :id', $this->table);
		$stmt = $this->db->prepare($sql);
		$stmt->execute([
			'f' => $$fname
		]);

		return $stmt->fetch();
	}
}