<?php
error_reporting(E_ALL);

use core\DBConnector;
use Model\ProductsModel;

function __autoload($classname) {
	include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
}

session_start();

define('ROOT', '/');

$params = explode('/', $_GET['q']);

//Setting controller and action rooting
$controller = isset($params[0]) && $params[0] !== '' ? $params[0] : 'products';

switch ($controller) {
	case 'products':
		$controller = 'Products';
		break;
	case '':
		$controller = '';
		break;
	default:
		$controller = 'Pages';
		break;
}

$action = '';
if ($controller !== 'Pages') {
	$fname = false;
	if (isset($params[1]) && is_numeric($params[1])) {
		$fname = $params[1];
		$params[1] = 'one';
	}
	$action = isset($params[1]) && $params[1] !== '' && is_string($params[1]) ? $params[1] : 'index';
	$action = sprintf('%sAction', $action);
	if (!$fname) {
		$fname = isset($params[2]) && is_numeric($params[2]) ? $params[2] : false;
	}
	if ($fname) {
		$_GET['fname'] = $fname;
	}
} else {
	$action = 'err404Action';
}

$request = new core\Request($_GET, $_POST, $_SERVER, $_COOKIE, $_FILES, $_SESSION);
$controller = sprintf('Controller\%sController', $controller);
if(!method_exists($controller, $action)) {
	$controller = 'Controller\\PagesController';
	$action = 'err404Action';
	header("HTTP/1.0 404 Not Found");
}
$controller = new $controller($request);
$controller->$action();
$controller->render();

