<?php

namespace core;

//Connecting to DB, using PDO;
class DBConnector
{
    protected static $db;

    public static function getConnect()
    {
        if(self::$db === null) {
            self::$db = self::getPDO();
        }
        return self::$db;
    }

    protected static function getPDO()
    {
        $dsn = sprintf('%s:host=%s;dbname=%s', 'mysql', 'localhost', 'products');
        return new \PDO($dsn, 'root', '');
    }

    public static function checkQuery($query)
    {
        if($query->errorCode() != \PDO::ERR_NONE) {
            $info = $query->errorInfo();
            echo implode('<br>', $info);
            exit();
        }
    }
}
