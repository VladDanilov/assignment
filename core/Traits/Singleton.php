<?php

namespace core\Traits;

use\core\DBConnector;

trait Singleton
{
    protected static $singleton_instance;

    public static function instance()
    {
        if(static::$singleton_instance===null) {
            static::$singleton_instance = new static(DBConnector::getConnect());
        }

        return static::$singleton_instance;
    }

    protected function __construct() {}
    protected function __clone() {}
    protected function __sleep() {}
    protected function __wakeup() {}
}