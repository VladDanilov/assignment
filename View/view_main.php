<body>
	<div class="main">	
		<?php foreach ($products as $product): ?>
				<div class="fields">
					<h2>
						<a href="/products/<?=$product['id']?>"><?=$product['SKU']?></a>
					</h2>
					<p>
						<?=$product['name']?>
					</p>
					<p>
						<?=$product['price'] . "$";?>
					</p>
						<?=(!empty($product['height'])) ? 'Dimensions:' : null ?>
						<?=(!empty($product['size'])) ? 'Size:' : null ?>
						<?=(!empty($product['weight'])) ? 'Weight:' : null ?>
					<p>
					<?=$product['height'];?>
					<?=(!empty($product['height'])) ? 'x' : null ?> 

					<?=$product['width'];?>
					<?=(!empty($product['length'])) ? 'x' : null ?> 

					 <?=$product['length'];?>
					 
					</p>
					<p>
						<?=$product['weight']?>
						<?=(!empty($product['weight'])) ? 'KG' : null ?>
					</p>

					<p>
						<?=$product['size']?>
						<?=(!empty($product['size'])) ? 'MB' : null ?>
					</p>
					
				</div>
		<?php endforeach;?>
	</div>
	<button class="addProduct"><a href="/products/add" class="Aadd">Add Product</a></button>
</body>
</html>
