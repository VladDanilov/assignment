<div class="post">
    <br><p style="color:red"><?=$err?></p><br>
        <form method="post">
            <p>SKU</p>                    
            <input type="text" name="SKU" value="<?=$SKU ?? null?>"><br>
            <p>Name</p>
            <input type="text" name="name" value="<?=$name ?? null?>"><br>
            <p>Price</p>
            <input type="text" name="price" value="<?=$price ?? null?>"><br>
            <br><select>
                            <option>--Select product type--</option>
                            <option value="size">Size</option>
                            <option value="dimensions">Dimensions</option>
                            <option value="weight">Weight</option>
            </select>
                    <div class="size item">
                        Size: <input name="size" type="text" value="<?=$size ?? null?>"/> <br />
                        <p><i>Size should be specified in MB</i></p>
                    </div> <br />
                    <div class="dimensions item">
                        Height: <input name="height" type="text" value="<?=$height ?? null?>"/> <br /> 
                        Width: <input name="width" type="text" value="<?=$width ?? null?>"/> <br /> 
                        Length: <input name="length" type="text" value="<?=$length ?? null?>"/> <br /> 
                        <p><i>Dimensions should be specified in HxWxL format</i></p>
                    </div> <br />
                    
                    <div class="weight item">
                    Weight: <input name="weight" type="text" value="<?=$weight ?? null?>"/> <br />
                    <p><i>Weight should be specified in kg</i></p> <br /> 
                    </div>
                    <input type="submit" value="Save" id="save">

        </form>
</div>
