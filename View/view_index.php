<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="<?=ROOT?>assists/css/Main.css">
<link rel="stylesheet" type="text/css" href="<?=ROOT?>assists/css/addproduct.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?=ROOT?>assists/js/addproduct.js"></script>
<title><?=$title?></title>
</head>
<body>
	<header class="header">
        <div class="header">
            <a href=""></a>
		</div>
	</header>
	<div class=Hnavbar>
		<nav class="navbar">
			<ul class="nav__links">

				<li><a href="#">Services</a></li>
				<li><a href="#">Projects</a></li>
				<li><a href="#">Services</a></li>
				<li><a href="#">Contact us</a></li>
			</ul>
		</nav>
	</div>
	<!-- Header End -->
	<div class="products">
			<?=$content?>
	</div>	
	<!-- Main End -->
	<footer>
		<div class="wrapper">
		<ul class="social_profiles">
				<li class="f"><a href="#" target="_blink"></a></li>
				<li class="t"><a href="#" target="_blink"></a></li>
		</ul>
		</div>
	</footer>
	<!-- Footer End -->
</body>
</html>