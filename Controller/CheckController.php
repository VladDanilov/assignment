<?php

namespace Controller;

class CheckController extends BaseController
{

	public static function checkContents($content)
	{
    	return (preg_match("/^[a-zA-Z0-9_ ]+$/", $content));
	}
    //Функция проверки валидности гет запроса
 	public static function checkFname($fname)
 	{
    	return (preg_match("/^[a-zA-Z0-9_ ]+$/", $fname));
	}

}