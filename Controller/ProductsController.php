<?php

namespace Controller;

use Model\ProductsModel as ProductsModel;
use core\DBConnector;

class ProductsController extends BaseController
{
	public function indexAction()
	{
    $mProduct = ProductsModel::instance();
  	$all = $mProduct->getAll();
	$this->title = 'Main';
    $this->content = $this->build(__DIR__ . BaseController::PATH . 'main.php',
    		[
  				'products' => $all
    		]
    	);
  	}
			
  	public function addAction()
	{
		$addProduct = ProductsModel::instance();
		$this->title .= 'Add product';
		if($this->request->isPost()) {
			$SKU = htmlspecialchars(trim($this->request->post('SKU')));
			$name = htmlspecialchars(trim($this->request->post('name')));
			$price = htmlspecialchars(trim($this->request->post('price')));
			$size = htmlspecialchars(trim($this->request->post('size')));
			$height = htmlspecialchars(trim($this->request->post('height')));
			$width = htmlspecialchars(trim($this->request->post('width')));
			$length = htmlspecialchars(trim($this->request->post('length')));
			$weight = htmlspecialchars(trim($this->request->post('weight')));
			  if($SKU == '' || $name == '' || $price == '') {
			  $err = 'Error: Please fill out all required fields!';
			  } elseif(!CheckController::checkContents($name)) {
				 $err = 'Invalid characters!';
			  } else {
				$addProduct->insert($SKU, $name, $price, $size, $height, $width, $length, $weight);
				header('Location: '. ROOT . 'products');
				exit();
			  }
		  } else {
			$SKU = '';
			$name = '';
			$price = '';
			$size = '';
			$height = '';
			$width = '';
			$length = '';
			$weight = '';
			$err = '';
		  }
			$this->content = $this->build(__DIR__ . BaseController::PATH . 'add.php', 
			[
				'SKU' => $SKU,
				'name' => $name,
				'price' => $price,
				'size' => $size,
				'height' => $height,
				'width' => $width,
				'length' => $length,
				'weight' => $weight,
				'err' => $err ?? null,
			]
		);
	}

// Tryied to create delete action, but it does not work properly 
	public function deleteAction()
	{
		$mProduct = ProductsModel::instance();
		$fname = $this->request->get('fname');
			if($fname === null) {
			exit("<strong>Error 404. Invalid parameter given!</strong>");
			} elseif (!CheckController::checkFname($fname)) {
			exit('Invalid action!');
			} else {
			$mProduct->delete($fname);
			}
			header('Location: '. ROOT . 'products');
			exit();
    }
}