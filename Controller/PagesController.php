<?php

namespace Controller;

use core\DBConnector;

class PagesController extends BaseController
{
	public function err404Action()
	{
    header("HTTP/1.0 404 Not Found");
    $this->title = 'Error 404';
    $this->content = $this->build(__DIR__ . BaseController::PATH . 'err404.php');
  	}
}
