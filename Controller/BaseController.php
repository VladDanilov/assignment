<?php

namespace Controller;

use core\Request;
use Model\ProductsModel;
use core\DBConnector;

abstract class BaseController
{

	const PATH = '/../View//view_';
	protected $title;
	protected $content;
	protected $request;
	protected $static;

	public function __construct(Request $request)
	{
		$mProduct = ProductsModel::instance();
    	$static = $mProduct->getAll();
		$this->request = $request;
		$this->title = '';
		$this->content = '';
		$this->static = $static;
	}

	public  function render()
	{
		echo $this->build(
			__DIR__ . self::PATH . 'index.php',
			[
				'title' => $this->title,
				'content' => $this->content,
				'static' => $this->static
			]
		);
	}
	//buffering view content
	protected function build($filename, array $params = [])
	{
		ob_start();
		extract($params);
		include_once $filename;
		return ob_get_clean();
	}
}
